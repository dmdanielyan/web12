var form = document.getElementById("form"); 
var btn = document.getElementsByClassName("contact__button")[0]; 
var btn1 = document.getElementsByClassName("contact__button")[1];
var close = document.getElementById("close"); 

$("#btn").click(function() {
  if($("#form").css('display') == "none") {
    $("#form").show(400);
    $("#form").css('display') == "fixed"
    $("#form").animate({
        left: 0 + "%"
      }, {
        duration: 4000,
        step: function(now, fx){
            $("#form").css( "left", now );
        }
        });
  }
  else {
    $("#form").hide(4000);
    $("#form").css('display') == "none"
  }
})

//При нажатии на кнопку 
btn.onclick = function() {
  history.pushState({page: 2}, "title 2", "?page=2"); 
  form.style.display = "flex"; 
} 
btn1.onclick = function() { 
  form.style.display = "flex"; 
} 
close.onclick = function() { 
  history.pushState({page: 1}, "title 1", "?page=1");
  form.style.display = "none"; 
} 

//При клике вне окна 
window.onclick = function(event) { 
  if (event.target == form) { 
  form.style.display = "none";
  history.pushState({page: 2}, "title 2", "?page=1");
  form.style.display = "none";
  } 
}


addEventListener("popstate",function(e){
    
    form.style.display = "none";
    history.pushState({page: 1}, "title 1", "?page=1");

}, true);

var send = document.getElementById("send")[0];


$(".ajaxForm").submit(function(e){ 
  e.preventDefault(); 
  var href = $(this).attr("action"); 
  $.ajax({ 
    type: "POST", 
    dataType: "json", 
    url: href, 
    data: $(this).serialize(), 
    success: function(response){ 
    if(response.status == "success"){ 
      alert("We received your submission, thank you!"); 
      localStorage.setItem("form", "myform") 
    }else{ 
      alert("An error occured: " + response.message); 
    } 
  } 
}); 
});


  
if (window.localStorage) { 
//Сохраняем данные input, textarea 
  var elements = document.querySelectorAll('[name]'); 
  for (var i = 0, length = elements.length; i < length; i++) { 
    (function(element) { 
      var name = element.getAttribute('name'); 
      element.value = localStorage.getItem(name) || ''; 
      element.onkeyup = function() { 
        localStorage.setItem(name, element.value); 
      };

    })(elements[i]); 
  } 
}


$("#close").click(function() {
  if($("#form").css('display') == "none") {
    $("#form").hide(400);
    $("#form").css('display') == "none"
    $("#form").animate({
        left: 0 + "%"
      }, {
        duration: 1500,
        step: function(now, fx){
            $("#form").css( "right", now);
        }
        });
  }
  else {
    $("#form").hide(400);
    $("#form").css('display') != "none"
  }
})




var input = document.getElementById("select_input");

if (input && input.value)
{
    // Send data from HTML-form to server

    // block button
    document.getElementById("submit__button").disabled = true;

    // перед отправкой надо собрать все имеющиеся данные (ФИО, регион и тд )
    let fio = document.getElementById("uniqueFIO").value;
    let phoneNumber = document.getElementById("uniquePhone").value;
    
    var e = document.getElementById("select_input");
    let region = e.options[e.selectedIndex].text;
    let message = document.getElementById("uniqeMessage").value;

    let data = {fio, phoneNumber, region, message};

    let options = {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }
    let request = fetch("https://formcarry.com/s/qoxJRZ3b5DA", options);

    var submit = document.getElementById("submit__button");
    submit.disabled = true;
    
} else {
    alert("Error. Please try again.");
}

